﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VertexController : MonoBehaviour 
{
	public bool Selected = false;

	public bool Dragable = false;

	public bool isDragging = false;

	public int SelectionIndex = -1;

	

	Ray ray; RaycastHit hit;


	
	Vector3 DragOffset = new Vector3();

	public void BeginDragging(Vector3 CoursorPosition)
	{
		isDragging = true;

		DragOffset = CoursorPosition - new Vector3(transform.position.x, 0f, transform.position.z);

		//Debug.Log(name + " : Started Dragging : " + CoursorPosition);
	} 

	public void KeepDragging(Vector3 CoursorPosition)
	{
		transform.position = CoursorPosition - DragOffset + Config.Data.Verticies.CorrectionOffset;

		//Debug.Log(name + " : Dragged : " + CoursorPosition);
	}
 
	public void StopDragging()
	{
		isDragging = false;

		//Debug.Log(name + " : Stopped dragging");
	} 
}
