﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour 
{
	public AudioListener audioListener;

	public AudioSource audioSource;

	public AudioClip HoverClip, ClickClip, SuccessClip, ErrorClip;

	public enum ClipType
	{
		Hover,
		Click,
		Success,
		Error
	}

	public void PlayHoverClip()
	{
		audioSource.Stop();

		if(HoverClip == null)
		{
			Debug.Log("SoundController : HoverClip is null)");

			return;
		}
			
		audioSource.clip = HoverClip;

		audioSource.Play();
	}

	public void PlayClickClip()
	{
		audioSource.Stop();

		if(HoverClip == null)
		{
			Debug.Log("SoundController : ClickClip is null)");

			return;
		}
			
		audioSource.clip = ClickClip;

		audioSource.Play();
	}

	public void PlayClip(ClipType clipType)
	{
		if(audioListener == null)
		{
			Debug.Log("SoundController : audiolistener is null");

			return;
		}
		
		if(audioSource == null)
		{
			Debug.Log("SoundController : audioSource is null");

			return;
		}
		
		switch(clipType)
		{
			default:
			
			PlayHoverClip();

			break;
		}
	}
}
