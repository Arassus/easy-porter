﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Input_Selection_Main_Select 
{
	public KeyCode ClickDown;
	public bool IsClickedDown() 
	{
		return Input.GetKeyDown(ClickDown);
	}

	public KeyCode Click;
	public bool IsClicked()
	{
		return Input.GetKey(Click);
	}

	public KeyCode ClickUp;
	public bool IsClickedUp()
	{
		return Input.GetKeyUp(ClickUp);
	}
}
