﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Input_Selection_Main 
{
	public Input_Selection_Main_Drag Drag; 

	public Input_Selection_Main_Select Select;
}
