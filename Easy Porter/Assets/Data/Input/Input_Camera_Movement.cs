﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Input_Camera_Movement
{
    public Input_Camera_Movement_Down Down;
    
    public Input_Camera_Movement_Left Left;
    
    public Input_Camera_Movement_Right Right;
    
    public Input_Camera_Movement_Up Up;
}
