﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Input_Selection_Additional
{
	public Input_Selection_Additional_One One;
	
	public Input_Selection_Additional_OneMore OneMore;
	
	public Input_Selection_Additional_To To; 

	public Input_Selection_Additional_All All;
}
