﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
	public class Input_Selection  
	{
		public Input_Selection_Main Main;

		public Input_Selection_Additional Additional;
	} 
