﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathsController : MonoBehaviour 
{
	public Transform PathPrefab;

	public List<PathController> PathList = new List<PathController>();


	string AdditionalMessage = "";

	public void GeneratePathsFromSelectedEdges()
	{
		List<EdgeController> SelectedEdges = Config.Data.Edges.GetSelectedEdges();

		AlertController Alert = Config.Data.Alert;

		bool EdgesAreProperlyConnected = EdgesProperlyConnected(SelectedEdges);

		if(EdgesAreProperlyConnected)
		{
			Transform NewPath = Instantiate(PathPrefab);

			NewPath.parent = transform;

			PathController Path = NewPath.GetComponent<PathController>();

			foreach (var edge in SelectedEdges)
			{
				edge.Paths.Add(Path);
			}

			Path.Edges = SelectedEdges;

			PathList.Add(Path);

			Alert.Initiate("Success", "Path has been properly generated" + AdditionalMessage);
		}
		else
		{
			Alert.Initiate("Failure", "Path has not been properly generated" + AdditionalMessage);
		}
	}

	public bool EdgesProperlyConnected(List<EdgeController> SelectedEdges) 
	{
		//Case for when none of the edges have been selected
		bool ProperlyConnected = SelectedEdges.Count > 0 ? true : false;

        int ACount = 0, BCount = 0, TipCount = 0;

		foreach (var edge in SelectedEdges)
		{
			ACount = 0; 
		
			BCount = 0;

			foreach (var neighbourEdge in SelectedEdges)
			{
				if(edge != neighbourEdge)
				{
					if(edge.A == neighbourEdge.A ||edge.A == neighbourEdge.B)
					{
						ACount+=1;
					}
					
					if(edge.B == neighbourEdge.A ||edge.B == neighbourEdge.B)
					{
						BCount+=1;
					}
				}
				else	//the first checked edge is the same one as the second
				{
					if(SelectedEdges.Count == 1)
					{
						ProperlyConnected = true;

						break;
					}
				}
			}

			if(ACount == 0)
			{
				TipCount += 1;
			}

			if(BCount == 0)
			{
				TipCount += 1;
			}

			if(TipCount > 2)
			{
				ProperlyConnected = false;

				AdditionalMessage = ": There are more than two tips of your path";

				break;
			}
		}

		return ProperlyConnected;
	}

	private EdgeController LastlySelectedEdge = null;

	private int LastlySelectedPathId = 0;

	public void Update_Coursor(bool CoursorCollidedWithObject, EdgeController CollidedEdge)
	{
		if(Config.SelectionConfig.SelectionMode == SelectionUIConfig.SelectionModeEnum.Path)
		{
			if (Config.Data.PorterInput.Selection.Main.Select.IsClickedDown())
            {
                if (CoursorCollidedWithObject)
                {
                    if (CollidedEdge == null)
                    {
						Config.Data.Edges.DeselectAllEdges();
                    }
                    else
                    {
						Config.Data.Edges.DeselectAllEdges();

						if(CollidedEdge == LastlySelectedEdge)
						{
							LastlySelectedPathId += 1;

							if(LastlySelectedPathId >= LastlySelectedEdge.Paths.Count)
							{
								LastlySelectedPathId = 0;
							}
						}
						else
						{
							LastlySelectedEdge = CollidedEdge;

							LastlySelectedPathId = 0;
						}

						Config.Data.Edges.SelectEdgesOfPath(CollidedEdge.Paths[LastlySelectedPathId]);
                    }
				}
			}
		}
	}
}
