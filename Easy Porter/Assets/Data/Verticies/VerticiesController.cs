﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticiesController : MonoBehaviour 
{
	public Transform VertexPrefab;
	public List<VertexController> VertexList = new List<VertexController>();


	public Vector3 InitialPosition;

	public Vector3 CorrectionOffset;

	public bool IsVectorLayingWithinOffset(Vector3 Position1, Vector3 Position2, float offset)
	{
		return (Position2.x - offset < Position1.x && Position1.x < Position2.x + offset) &&
				(Position2.y - offset < Position1.y && Position1.y < Position2.y + offset) &&
				(Position2.z - offset < Position1.z && Position1.z < Position2.z + offset);
	}

	public bool IsAnyVertexAtPosition(ref Vector3 Position)
	{
		bool answer = false;

		foreach(VertexController vertex in VertexList)
		{
			Vector3 OldPosition = vertex.transform.position;

			answer = IsVectorLayingWithinOffset(Position, OldPosition, Config.Data.Map.Grid);

			if(answer == true)
			{
				// Debug.Log("true : " + Position.ToString() + " : " + OldPosition.ToString());
				
				break;
			}
			else
			{
				// Debug.Log("false : " + Position.ToString() + " : " + OldPosition.ToString());
			}
		}

		return answer;
	}

	public Vector3 GetUniquePosition()
	{
		Vector3 NewPosition = InitialPosition ;

		while(IsAnyVertexAtPosition(ref NewPosition))
		{
			NewPosition = NewPosition + new Vector3(Config.Data.Map.Grid, 0f,0f);
		}

		return NewPosition;
	}

	public List<VertexController> Selected() 
	{
		List<VertexController> SelectedVerticies = new List<VertexController>();

		for(int i=0; i<SelectedCount; i++)
		{
			foreach (var vertex in VertexList)
			{
				if(vertex.SelectionIndex == i)
				{
					SelectedVerticies.Add(vertex);
				}
			}
		}

		return SelectedVerticies;
	}

	
	public void AddVertex()
	{
		if(VertexPrefab == null)
		{
			Debug.Log("DataController : Vertex Prefab is null");

			return;
		}

		Transform NewVertex = Instantiate(VertexPrefab);

		if(NewVertex == null)
		{
			Debug.Log("DataController : New Vertex has not been instantiated successfully");

			return;
		}

		NewVertex.parent = gameObject.transform;

		NewVertex.name = "Vert " + Time.time.ToString();

		NewVertex.position = GetUniquePosition();

		VertexController NewVertexController = NewVertex.GetComponent<VertexController>();

		if(NewVertexController == null)
		{
			Debug.Log("DataController : Instantiated Vertex does not have the VertexController Component");	
			
			return;			
		}

		VertexList.Add(NewVertexController);

		recentVertex = NewVertexController;

		DescriptionsController description = Config.Data.Description;

		List<DescriptionsController.Action> CancelActions = new List<DescriptionsController.Action>() {DestroyRecentVertex};

		List<DescriptionsController.Action> XActions = new List<DescriptionsController.Action>() {DestroyRecentVertex};

		List<DescriptionsController.Action> SaveActions = new List<DescriptionsController.Action>() {SaveVertex};

	
		description.Initiate("Add Vertex", description.AddVertexPage, SaveActions, null, CancelActions, XActions);
	}

	VertexController recentVertex = null;

	private void DestroyRecentVertex()
	{
		int id = VertexList.IndexOf(recentVertex);

		VertexList.RemoveAt(id);

		Destroy(recentVertex.gameObject);

		Config.Data.Edges.RemoveDisconnectedEdges();

		Config.Data.Description.Panel.gameObject.SetActive(false);
	}

	private void SaveVertex()
	{
		Config.Data.Description.Panel.gameObject.SetActive(false);
	}



	public int SelectedCount = 0;

	private void RefreshSelected()
	{
		int NewCount = 0;

		foreach(VertexController vertex in VertexList)
		{
			if(vertex.Selected)
			{
				NewCount++;
			}
		}
	}

	public void RemoveAllVerticies()
	{
		VertexList.Clear();

		int ChildCount = this.transform.childCount;

		for(int i=0 ; i<ChildCount ; i++)
		{
			Destroy(transform.GetChild(i).gameObject);
		}

		Config.Data.Edges.RemoveDisconnectedEdges();
	}
	
	public void RemoveSelectedVerticies()
	{
		int ChildCount = this.transform.childCount;

		for(int i=0, j=0; i<ChildCount ; i++, j++)
		{
			if(VertexList[j].Selected)
			{
				VertexList.RemoveAt(j);

				j--;

				Destroy(transform.GetChild(i).gameObject);
			}
		}

		Config.Data.Edges.RemoveDisconnectedEdges();
	}
	
	public void RemoveDeselectedVerticies()
	{
		int ChildCount = this.transform.childCount;

		for(int i=0, j=0; i<ChildCount ; i++, j++)
		{
			if(!VertexList[j].Selected)
			{
				VertexList.RemoveAt(j);

				j--;

				Destroy(transform.GetChild(i).gameObject);
			}
		}

		Config.Data.Edges.RemoveDisconnectedEdges();
	}

	public void SelectAllVerticies()
	{
		foreach (VertexController vertex in VertexList)
		{
			SelectVertex(vertex);
		}
	}

	public void DeselectAllVerticies()
	{
		foreach (VertexController vertex in VertexList)
		{
			DeselectVertex(vertex);
		}
	}

	 void SelectVertex(VertexController vertex)
	{
		vertex.Selected = true;

		vertex.SelectionIndex = SelectedCount;

		Renderer vertexRenderer = vertex.GetComponent<Renderer>();

		vertexRenderer.material.SetFloat("_Selected", 1f);

		Refresh_SelectedIndex();

		Refresh_Count();

		SelectionChanged();
	}

	 void DeselectVertex(VertexController vertex)
	{
		vertex.Selected = false;

		vertex.SelectionIndex = -1;

		Renderer vertexRenderer = vertex.GetComponent<Renderer>();

		vertexRenderer.material.SetFloat("_Selected", 0f);

		Refresh_SelectedIndex();

		Refresh_Count();

		SelectionChanged();
	}

	void SelectionChanged()
	{
		Config.Data.SelectionChangedInVerticies();
	}

	void Refresh_Count()
	{
		int NewSelectedCount = 0;

		foreach(VertexController vertex in VertexList)
		{
			if(vertex.Selected)
			{
				NewSelectedCount += 1;
			}
		}

		SelectedCount = NewSelectedCount;
	}

	public void SelectOneVertex(VertexController vertex)
	{
		DeselectAllVerticies();

		SelectVertex(vertex);
	}

	public void AddToVertexSelection(VertexController vertex)
	{
		SelectVertex(vertex);
	}

	public void Refresh_SelectedIndex()
	{
		int IndexOffset = 0;

		for(int i=0; i<SelectedCount; i++)
		{
			bool FoundSelectedIndex = false;

			VertexController CheckedVertex = null;

			foreach(VertexController vertex in VertexList)
			{
				if(vertex.SelectionIndex == i)
				{
					FoundSelectedIndex = true;

					CheckedVertex = vertex;
				}
			}

			if(FoundSelectedIndex)
			{
				CheckedVertex.SelectionIndex -= IndexOffset; 
			}
			else
			{
				IndexOffset++;
			}
		}
	}

	public VertexController GetVertex_By(int SelectionIndex)
	{
		VertexController FoundVertex = null;

		foreach (VertexController vertex in VertexList)
		{
			if(vertex.SelectionIndex == SelectionIndex)
			{
				// Debug.Log("Vertex with selected index of '" + SelectionIndex.ToString() + "' has been found => '" + VertexList.IndexOf(vertex).ToString() + "'(ID)");
				
				FoundVertex = vertex;
			}
		}

		if(FoundVertex == null)
		{
			// Debug.Log("Vertex with selected index of '" + SelectionIndex.ToString() + "' has not been found");
		}

		return FoundVertex;
	}

	public void BeginDraggingAllSelectedElements()
	{
		foreach(VertexController vertex in VertexList)
		{
			if(vertex.Selected && vertex.Dragable)
			{
				vertex.BeginDragging(Config.Data.hit.point);
			}
		}
	}

	public void KeepDraggingAllSelectedElements()
	{
		foreach(VertexController vertex in VertexList)
		{
			if(vertex.isDragging)
			{
				vertex.KeepDragging(Config.Data.hit.point);
			}
		}
	}
	
	public void StopDraggingAllSelectedElements()
	{
		foreach(VertexController vertex in VertexList)
		{
			if(vertex.isDragging)
			{
				vertex.StopDragging();
			}
		}
	}
	
	private float ClickCooldownTime = 0.5f, PassedClickTime = 0f;

	public void Update_Coursor(bool CoursorCollidedWithObject , VertexController CollidedVertex = null)
	{
		///If collides with nothing then Drag
		if(CollidedVertex == null)
		{
			if(Config.Data.PorterInput.Selection.Main.Drag.IsClickedUp())
			{
				// Debug.Log("Drag Up");
				
				StopDraggingAllSelectedElements();
			}

			if(Config.Data.PorterInput.Selection.Main.Drag.IsClicked())
			{
				// Debug.Log("Drag");
				
				KeepDraggingAllSelectedElements();
			}

			if(Config.Data.PorterInput.Selection.Main.Drag.IsClickedDown())
			{
				// Debug.Log("Drag Down");
				
				BeginDraggingAllSelectedElements();
			}
		}

		// constant selection reset
        if (Config.Data.PorterInput.Selection.Main.Select.IsClickedUp())
        {
            PassedClickTime = 0f;
        }

		// perform constant selection
        if (Config.Data.PorterInput.Selection.Main.Select.IsClicked())
        {
            PassedClickTime += Time.deltaTime;

            if (PassedClickTime > ClickCooldownTime)
            {
                if (CoursorCollidedWithObject)
                {
                    if (CollidedVertex == null)
                    {
                        //Debug.Log("object is not a vertex");
                    }
                    else
                    {
                        if (!CollidedVertex.Selected)
                        {
                            AddToVertexSelection(CollidedVertex);
                        }
                    }
                }
            }
        }

		// select + initiate constant selection
        if (Config.Data.PorterInput.Selection.Main.Select.IsClickedDown())
        {
            PassedClickTime = 0f;

            if (CoursorCollidedWithObject)
            {
                if (CollidedVertex == null)
                {
                    DeselectAllVerticies();
                }
                else
                {
                    if (Config.Data.PorterInput.Selection.Additional.OneMore.IsClicked())
                    {
                        if (!CollidedVertex.Selected)
                        {
                            AddToVertexSelection(CollidedVertex);
                        }
                        else
                        {
                            DeselectVertex(CollidedVertex);
                        }
                    }
                    else
                    {
                        if (Config.Data.PorterInput.Selection.Additional.To.IsClicked())
                        {
                            //SelectToVertex(vertex);
                            SelectOneVertex(CollidedVertex);
                        }
                        else
                        {
                            SelectOneVertex(CollidedVertex);
                        }
                    }
                }
            }
            else
            {
                DeselectAllVerticies();
            }
        }
	}

	public void Update_Keyboard()
	{
		if(Config.Data.PorterInput.Selection.Additional.All.IsClickedDown())
		{
			bool SelectAll = true;

			foreach(VertexController vertex in VertexList)
			{
				if(vertex.Selected)
				{
					SelectAll = false;

					break;
				}
			}

			if(SelectAll) 
			{
				SelectAllVerticies();
			} 
			else
			{
				DeselectAllVerticies();
			}
		}
	}

	public void SetupForSelectionMode_Vertex()
	{
		foreach (var vertex in VertexList)
		{
			MeshRenderer rend = vertex.GetComponent<MeshRenderer>();
			rend.enabled = true;

			BoxCollider box = vertex.GetComponent<BoxCollider>();
			box.enabled = true;
		} 

		DeselectAllVerticies();
	}

	public void SetupForSelectionMode_Edge()
	{
		foreach (var vertex in VertexList)
		{
			MeshRenderer rend = vertex.GetComponent<MeshRenderer>();
			rend.enabled = false;

			BoxCollider box = vertex.GetComponent<BoxCollider>();
			box.enabled = false;
		}

		DeselectAllVerticies();
	}
	

	public void SetupForSelectionMode_Path()
	{
		foreach (var vertex in VertexList)
		{
			MeshRenderer rend = vertex.GetComponent<MeshRenderer>();
			rend.enabled = false;

			BoxCollider box = vertex.GetComponent<BoxCollider>();
			box.enabled = false;
		}

		DeselectAllVerticies();
	}


	public void DisplayDescription()
	{
		//vert Id + proccesses
	}
}
