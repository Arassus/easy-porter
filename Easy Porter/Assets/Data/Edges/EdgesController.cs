﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgesController : MonoBehaviour 
{
	public Transform EdgePrefab; 

	public List<EdgeController> EdgeList = new List<EdgeController>();

	public void GenerateEdgesFromSelectedVericies()
	{
		for(int i=1; i<Config.Data.Verticies.SelectedCount; i++)
		{
			VertexController A = Config.Data.Verticies.GetVertex_By(i - 1);

			VertexController B = Config.Data.Verticies.GetVertex_By(i);

			GenerateEdge(A, B);
		}
	}

	public void GenerateEdge(VertexController A, VertexController B)
    {
		Transform NewObject = Instantiate(EdgePrefab);

        EdgeController newEdge = NewObject.GetComponent<EdgeController>();

        newEdge.Connect(A, B);

        newEdge.transform.parent = transform;

        EdgeList.Add(newEdge);

        Refresh_Selection();
	}

	public void RemoveDisconnectedEdges()
	{
		int ChildCount = this.transform.childCount;

		for(int i=0, j=0; i<ChildCount ; i++, j++)
		{
			if(EdgeList[j].A == null || EdgeList[j].B == null)
			{
				for(int ii=0, jj=0; ii < Config.Data.Paths.PathList.Count; ii++, jj++)
				{
					PathController Path = Config.Data.Paths.PathList[jj];

					if(Path.Edges.Contains(EdgeList[j]))
					{
						EdgeList.RemoveAt(jj);

						jj--;

						Destroy(transform.GetChild(ii).gameObject);
					}
				}

				EdgeList.RemoveAt(j);

				j--;

				Destroy(transform.GetChild(i).gameObject);
			}
		}
	}

	public int SelectedCount = 0;

	public void Update_Coursor(bool CoursorCollidedWithObject, EdgeController CollidedEdge)
	{
		if(Config.SelectionConfig.SelectionMode == SelectionUIConfig.SelectionModeEnum.Edge)
		{
			if (Config.Data.PorterInput.Selection.Main.Select.IsClickedDown())
            {
                if (CoursorCollidedWithObject)
                {
                    if (CollidedEdge == null)
                    {
						DeselectAllEdges();
                    }
                    else
                    {
						if(Config.Data.PorterInput.Selection.Additional.OneMore.IsClicked())
						{
							SelectEdge(CollidedEdge);
						}
						else
						{
							SelectJustOneEdge(CollidedEdge);	
						}
                    }
                }
            }
		}
	}

	public void Refresh_SelectedIndex()
	{
		// int IndexOffset = 0;

		// for(int i=0; i<SelectedCount; i++)
		// {
		// 	bool FoundSelectedIndex = false;

		// 	VertexController CheckedVertex = null;

		// 	foreach(VertexController vertex in VertexList)
		// 	{
		// 		if(vertex.SelectionIndex == i)
		// 		{
		// 			FoundSelectedIndex = true;

		// 			CheckedVertex = vertex;
		// 		}
		// 	}

		// 	if(FoundSelectedIndex)
		// 	{
		// 		CheckedVertex.SelectionIndex -= IndexOffset; 
		// 	}
		// 	else
		// 	{
		// 		IndexOffset++;
		// 	}
		// }
	}

	// void SelectionChanged()
	// {
	// 	Config.Data.SelectionChangedInVerticies();
	// }

	void Refresh_Count()
	{
		// int NewSelectedCount = 0;

		// foreach(VertexController vertex in VertexList)
		// {
		// 	if(vertex.Selected)
		// 	{
		// 		NewSelectedCount += 1;
		// 	}
		// }

		// SelectedCount = NewSelectedCount;
	}

	public void SelectJustOneEdge(EdgeController CollidedEdge)
	{
        DeselectAllEdges();

        SelectEdge(CollidedEdge);
	}

	public void SelectEdgesOfPath(PathController Path)
	{
		foreach (var edge in EdgeList)
		{
			foreach (var InnerPath in edge.Paths)
			{
				if(InnerPath == Path)
				{
					SelectEdge(edge);
				}
			}
		}
	}

	public void DeselectAllEdges()
	{
		foreach (var edge in EdgeList)
		{
			DeselectEdge(edge);
		}
	}

	public void SelectAllEdges()
	{
		foreach (var edge in EdgeList)
		{
			SelectEdge(edge);
		}
	}

	private int selectionState = 0;
	public void SelectEdge(EdgeController edge)
	{
		edge.Selected = true;

		selectionState = 1;

		SetEdgeSelectionInMaterial(edge);
	}
	public void DeselectEdge(EdgeController edge)
	{
		edge.Selected = false;

		selectionState = 0;

		SetEdgeSelectionInMaterial(edge);
	}

	private void SetEdgeSelectionInMaterial(EdgeController edge)
	{
		edge.GetComponent<Renderer>().material.SetFloat("_Selected",selectionState);
	}

	public void Refresh_Selection()
	{
		foreach (EdgeController edge in EdgeList)
		{
			if(edge.A.Selected && edge.B.Selected)
			{
				SelectEdge(edge);
			}
			else
			{
				DeselectEdge(edge);
			}
		}
	}

	public List<EdgeController> GetSelectedEdges()
	{
		List<EdgeController> SelectedEdges = new List<EdgeController>();

		foreach (var edge in EdgeList)
		{
			bool Selected = false;

			if(edge.Selected)
			{
				Selected = true;
			}

			if(edge.A.Selected && edge.B.Selected)
			{
				Selected = true;
			}

			if(Selected)
			{
				SelectedEdges.Add(edge);
			}
		}

		return SelectedEdges;
	}

	public EdgeController GetByVerticies(VertexController A, VertexController B)
	{
		foreach (var edge in EdgeList)
		{
			if((edge.A == A && edge.B == B)||(edge.A == B && edge.B == A))
			{
				return edge;
			}
		}

		return null;
	}



	public void SetupForSelectionMode_Vertex()
	{
		foreach (var edge in EdgeList)
		{
			LineRenderer rend = edge.GetComponent<LineRenderer>();
			rend.enabled = true;

			CapsuleCollider box = edge.GetComponent<CapsuleCollider>();
			box.enabled = true;
		}
	}

	public void SetupForSelectionMode_Edge()
	{
		foreach (var edge in EdgeList)
		{
			LineRenderer rend = edge.GetComponent<LineRenderer>();
			rend.enabled = true;

			CapsuleCollider box = edge.GetComponent<CapsuleCollider>();
			box.enabled = true;
		}
	}

	public void SetupForSelectionMode_Path()
	{
		foreach (var edge in EdgeList)
		{
			bool EdgeIsPArtOfAtLeastOnePath = (edge.Paths.Count > 0) ? true : false;

			LineRenderer rend = edge.GetComponent<LineRenderer>();
			rend.enabled = EdgeIsPArtOfAtLeastOnePath;

			CapsuleCollider box = edge.GetComponent<CapsuleCollider>();
			box.enabled = EdgeIsPArtOfAtLeastOnePath;
		}
	}
}
