﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DescriptionsController : MonoBehaviour
{
    public delegate void Action();

	public List<Action> SaveActions = new List<Action>();
	public List<Action> RemoveActions = new List<Action>();
	public List<Action> CancelActions = new List<Action>();
	public List<Action> XActions = new List<Action>();


    public Image Panel;

	public Text Header;

	public GameObject AddVertexPage;

	public Button SaveButton;

	public Button RemoveButton;

	public Button CancelButton;

	public Button XButton;
	

	public void CloseDescription()
	{
		Panel.gameObject.SetActive(false);
	}

	public void Initiate(string DescriptionHeader)
	{
		List<Action> x = new List<Action>(){ CloseDescription};

		Initiate(DescriptionHeader,null,null,null,null,x);
	}

	public void Initiate(string DescriptionHeader, GameObject Page, List<Action> Save, List<Action> Remove, List<Action> Cancel,List<Action> X)
	{
		if(this.Header == null)
		{
			Debug.Log("Description header object has not been assgned");

			return;
		}

		Header.text = DescriptionHeader;

		if(Page != null)
		Page.gameObject.SetActive(true);

		
		Panel.gameObject.SetActive(true);

		
		if(X != null)
		{
			foreach (var action in X)
			{
				XActions.Add(action);
			}

			XButton.gameObject.SetActive(true);
		}
		else
		{
			XButton.gameObject.SetActive(false);
		}	

		
		if(Cancel != null)
		{
			foreach (var action in Cancel)
			{
				CancelActions.Add(action);
			}

			CancelButton.gameObject.SetActive(true);
		}
		else
		{
			CancelButton.gameObject.SetActive(false);
		}

		
		if(Remove != null)
		{
			foreach (var action in Remove)
			{
				RemoveActions.Add(action);
			}

			RemoveButton.gameObject.SetActive(true);
		}
		else
		{
			RemoveButton.gameObject.SetActive(false);
		}

		
		if(Save != null)
		{
			foreach (var action in Save)
			{
				SaveActions.Add(action);
			}

			SaveButton.gameObject.SetActive(true);
		}
		else
		{
			SaveButton.gameObject.SetActive(false);
		}
	}

	public void Perform_X()
	{
		foreach (var Action in XActions)
		{
			Action();
		}

		XActions.Clear();
	}

	public void Perform_Cancel()
	{
		foreach (var Action in CancelActions)
		{
			Action();
		}

		CancelActions.Clear();
	}

	public void Perform_Remove()
	{
		foreach (var Action in RemoveActions)
		{
			Action();
		}

		RemoveActions.Clear();
	}

	public void Perform_Save()
	{
		foreach (var Action in SaveActions)
		{
			Action();
		}

		SaveActions.Clear();
	}
}
