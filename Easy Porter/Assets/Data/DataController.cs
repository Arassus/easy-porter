﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataController : MonoBehaviour 
{
	private void OnEnable() 
	{
		Config.Data = this;

		Verticies.SetupForSelectionMode_Vertex();	
	}


	public VerticiesController Verticies;

	public EdgesController Edges;

	public MapController Map; 

	public SoundController Sound;

	public InputController PorterInput;

	public PathsController Paths;

	public AlertController Alert;

	public DescriptionsController Description;


	public Ray ray; 
	
	public RaycastHit hit;

	public bool CoursorCollidedWithObject;


	private void Update() 
	{
		ray = Config.Data.Map.MainCamera.ScreenPointToRay(Input.mousePosition);

		CoursorCollidedWithObject = Physics.Raycast(ray, out hit);

		if (CoursorCollidedWithObject)
		{
				Update_Coursor();
		}
	}

	private void Update_Coursor()
	{
		if(Verticies != null && Config.SelectionConfig != null)
		{
			if(Config.SelectionConfig.SelectionMode == SelectionUIConfig.SelectionModeEnum.Vertex)
			{
				Verticies.Update_Coursor(CoursorCollidedWithObject, hit.collider.GetComponent<VertexController>());
				
				Verticies.Update_Keyboard();
			}
		}

		if(Edges != null && Config.SelectionConfig != null)
		{
			if(Config.SelectionConfig.SelectionMode == SelectionUIConfig.SelectionModeEnum.Edge)
			{
				Edges.Update_Coursor(CoursorCollidedWithObject, hit.collider.GetComponent<EdgeController>());
			}
		}

		if(Paths != null && Config.SelectionConfig != null)
		{
			if(Config.SelectionConfig.SelectionMode == SelectionUIConfig.SelectionModeEnum.Path)
			{
				Paths.Update_Coursor(CoursorCollidedWithObject, hit.collider.GetComponent<EdgeController>());
			}
		}
	}

	public void SelectionChangedInVerticies()
	{
		Edges.Refresh_Selection();
	}
}
