﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlertController : MonoBehaviour 
{
	public delegate void Action();

	public List<Action> OkActions = new List<Action>();
	public List<Action> YesActions = new List<Action>();
	public List<Action> NoActions = new List<Action>();

	public Image AlertPanel = null;
	public Text AlertHeader = null;
	public Text AlertDescription = null;

	public Button OkButton = null;
	public Button YesButton = null;
	public Button NoButton = null;

	public void Initiate(string AlertHeader, string AlertDescription)
	{
		List<Action> Ok = new List<Action>(){ CloseAlert};

		Initiate(AlertHeader,AlertDescription,Ok,null,null);
	}

	public void Initiate(string AlertHeader, string AlertDescription, List<Action> Ok, List<Action> Yes, List<Action> No)
	{
		if(this.AlertHeader == null)
		{
			Debug.Log("Alert header object has not been assgned");

			return;
		}

		this.AlertHeader.text = AlertHeader;
		
		if(this.AlertDescription == null)
		{
			Debug.Log("Alert description object has not been assgned");

			return;
		}
		
		this.AlertDescription.text = AlertDescription;

		AlertPanel.gameObject.SetActive(true);

		if(Ok != null)
		{
			foreach (var action in Ok)
			{
				OkActions.Add(action);
			}

			OkButton.gameObject.SetActive(true);
		}
		else
		{
			OkButton.gameObject.SetActive(false);
		}
		
		if(Yes != null)
		{
			foreach (var action in Yes)
			{
				YesActions.Add(action);
			}

			YesButton.gameObject.SetActive(true);
		}
		else
		{
			YesButton.gameObject.SetActive(false);
		}
		
		if(No != null)
		{
			foreach (var action in No)
			{
				NoActions.Add(action);
			}

			NoButton.gameObject.SetActive(true);
		}
		else
		{
			NoButton.gameObject.SetActive(false);
		}
	}

	private void CloseAlert()
	{
		AlertPanel.gameObject.SetActive(false);
	}

	public void Perform_Ok()
	{
		foreach (var Action in OkActions)
		{
			Action();
		}

		OkActions.Clear();
	}

	public void Perform_Yes()
	{
		foreach (var Action in YesActions)
		{
			Action();
		}

		OkActions.Clear();
	}

	public void Perform_No()
	{
		foreach (var Action in NoActions)
		{
			Action();
		}

		OkActions.Clear();
	}

}
