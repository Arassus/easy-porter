﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionUIConfig : MonoBehaviour 
{
	private void OnEnable() 
	{
		Config.SelectionConfig = this;;
	}

	private void Start() 
	{
		transform.parent.gameObject.SetActive(false);
	}

	public enum SelectionModeEnum
	{
		Vertex,
		Edge,
		Path
	} 

	public SelectionModeEnum SelectionMode = SelectionModeEnum.Vertex;

	public void SetSelectionMode_Vertex()
	{
		SelectionMode = SelectionModeEnum.Vertex;

		Config.Data.Verticies.SetupForSelectionMode_Vertex();

		Config.Data.Edges.SetupForSelectionMode_Vertex();
	}

	public void SetSelectionMode_Edge()
	{
		SelectionMode = SelectionModeEnum.Edge;

		Config.Data.Verticies.SetupForSelectionMode_Edge();

		Config.Data.Edges.SetupForSelectionMode_Edge();
	}

	public void SetSelectionMode_Path()
	{
		SelectionMode = SelectionModeEnum.Path;

		Config.Data.Verticies.SetupForSelectionMode_Path();

		Config.Data.Edges.SetupForSelectionMode_Path();
	}
}
 