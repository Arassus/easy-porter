﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RangeOptionInputController : MonoBehaviour 
{
	public string BaseName = null;
	public List<Text> Output;

	public List<Slider> Input;

	private void Start() 
	{
		if(string.IsNullOrEmpty(BaseName) && Output.Count > 0)
		{
			BaseName = Output[0].text;
		}
		else
		{
			Output[0].text = BaseName;
		}
	}

	private void Update() 
	{
		foreach(Text output in Output)
		{
			string NewOutput = BaseName + " : ";

			bool FirstScrollbar = true;

			foreach(Slider input in Input)
			{
				if(input != null)
					NewOutput += FirstScrollbar == true ? input.value.ToString() :  input.value.ToString() + ";";
			}

			if(output != null)
				output.text = NewOutput;
		}
	}
}