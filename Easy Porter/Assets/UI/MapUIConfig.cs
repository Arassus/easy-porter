﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapUIConfig : MonoBehaviour 
{
	private void OnEnable() 
	{
		Config.MapConfig = this;
	}

	public UnityEngine.UI.Slider GridSlider;

	public void RefreshGrid()	
	{
		Config.Data.Map.Grid = GridSlider.value;
	}

	private void Start() 
	{
		RefreshGrid();
	}
}
