﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TextOptionInputController : MonoBehaviour 
{
	public string BaseName = null;
	public List<Text> Output;
	
	// public List<InputField> Input;
	
	void Start () 
	{
		if(string.IsNullOrEmpty(BaseName) && Output.Count > 0)
		{
			BaseName = Output[0].text;
		}
		else
		{
			Output[0].text = BaseName;
		}
	}

	void Update () 
	{

	}
}
