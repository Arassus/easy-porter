﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DijkstraController : MonoBehaviour 
{
	List<VertexController> SelectedVerticies = new List<VertexController>();
	VertexController Start = null;
	VertexController End = null;
	List<VertexController> Verticies = new List<VertexController>();
	List<VertexController> Garbage = null;
	List<EdgeController> Edges = null;
	VertexController CurrentlyUsedVertex = null;
	List<VertexController> PotentialVerticies = new List<VertexController>();
	float PotentialLength;
	List<VertexController>[] Recent;
	float[] Weights;

	public void SelectShortestPathFromSelectedVericies()
	{
		bool CorrectSetup = CheckForCorrectSetup();

		if(!CorrectSetup)
		{
			return;
		}

		Initialize();

		Calculate();

		Cleanup();
	}

	private bool CheckForCorrectSetup()
	{
		if(Config.Data.Verticies.SelectedCount < 2)
		{
			Config.Data.Alert.Initiate("Could not generate paths", "You have to select at least 2 verticies");

			return false;
		}

		return true;
	}

	private void Initialize()
	{
		Debug.Log("Initializing Dijkstra alghorithm . . .");

		SelectedVerticies = Config.Data.Verticies.Selected();

		Start = SelectedVerticies[0];

		End = SelectedVerticies[1];

		Verticies = new List<VertexController>();
		foreach (var vertex in Config.Data.Verticies.VertexList)
		{
			Verticies.Add(vertex);
		}

		Garbage = new List<VertexController>();

		Edges = new List<EdgeController>();
		foreach (var edge in Config.Data.Edges.EdgeList)
		{
			Edges.Add(edge);
		}

		Recent = new List<VertexController>[Verticies.Count];
		for(int i=0; i<Recent.Length; i++)
		{
			if(Verticies[i] == Start)
			{
				Recent[i] = null; 
			}
			else
			{
				Recent[i] = new List<VertexController>();
			}
		}

		Weights = new float[Verticies.Count];
		for(int i=0;i<Weights.Length;i++)
		{
			if(Verticies[i] == Start)
			{
				Weights[i] = 0f; 
			}
			else
			{
				Weights[i] = float.MaxValue;
			}
		}

		PotentialVerticies = new List<VertexController>();
	}


	private void Calculate()
	{
		Debug.Log("Calculating Dijkstra alghorithm . . .");

		if(Verticies == null)
		Debug.Log("Verticies = null");
		
		if(Garbage == null)
		Debug.Log("Garbage = null");

		while(Verticies.Count > Garbage.Count)
		{
            EstablishNextVertexFromList();

            EstablishConnectedVerticies();

            RefreshWeightsAndRecentVerticies();

            SetupForNextStep();
		}

		GeneratePath();
	}

	void EstablishNextVertexFromList()
	{
		float ShortestSegment = float.MaxValue;

		for(int i=0; i<Weights.Length; i++)
		{
			if(Weights[i] < ShortestSegment && !Garbage.Contains(Verticies[i]))
			{
				// Debug.Log("Weight[" + i.ToString() + "] : " + Weights[i].ToString() + " : " + Garbage.Contains(Verticies[i]));

				ShortestSegment = Weights[i];

				CurrentlyUsedVertex = Verticies[i];	
			}
		}
	}

	void EstablishConnectedVerticies()
	{
		PotentialVerticies.Clear();
		
		foreach (var edge in Edges)
		{
			if(edge.A == CurrentlyUsedVertex && !Garbage.Contains(edge.B))
			{
				PotentialVerticies.Add(edge.B);
			}

			if(edge.B == CurrentlyUsedVertex && !Garbage.Contains(edge.A))
			{
				PotentialVerticies.Add(edge.A);
			}
		}
	}

	void RefreshWeightsAndRecentVerticies()
	{
		foreach (var potentialVertex in PotentialVerticies)
		{
			PotentialLength = Weights[Verticies.IndexOf(CurrentlyUsedVertex)] + Vector3.Magnitude(CurrentlyUsedVertex.transform.position - potentialVertex.transform.position);

			int indexOfPotentialVertex = Verticies.IndexOf(potentialVertex);

			// Debug.Log("Potential : " + Weights[indexOfPotentialVertex].ToString() + " : " + PotentialLength.ToString());

			if(Weights[indexOfPotentialVertex] > PotentialLength)
			{
				Weights[indexOfPotentialVertex] = PotentialLength;

				Recent[indexOfPotentialVertex].Clear();

				Recent[indexOfPotentialVertex].Add(CurrentlyUsedVertex);
			}
		};
	}

	void SetupForNextStep()
	{
		Garbage.Add(CurrentlyUsedVertex);
	}

	void GeneratePath()
	{
		int indexOfEndVertex = Verticies.IndexOf(End);

		List<EdgeController> FinalPath = new List<EdgeController>();

		while(true)
		{
			if(Recent[indexOfEndVertex] == null)
			{
				break;
			}

			int indexOfStartVertex = Verticies.IndexOf(Recent[indexOfEndVertex][0]);

            VertexController A = Verticies[indexOfEndVertex];

            VertexController B = Verticies[indexOfStartVertex];
			
			FinalPath.Add(Config.Data.Edges.GetByVerticies(A, B));

			indexOfEndVertex = Verticies.IndexOf(B);
		}

		foreach (var item in FinalPath)
		{
			Config.Data.Edges.SelectEdge(item);
		}

		// while(true)
		// {
		// 	if(Recent[indexOfEndVertex] == null)
		// 	{
		// 		break;
		// 	}

        //     int indexOfStartVertex = Verticies.IndexOf(Recent[indexOfEndVertex][0]);

		// 	VertexController A = Verticies[indexOfEndVertex];

		// 	VertexController B = Verticies[indexOfStartVertex];

		// 	EdgeController AB = Config.Data.Edges.GetByVerticies(A, B);

		// 	if(AB == null)
        //     {
		// 		Debug.Log(A.name + " : " + indexOfEndVertex.ToString());
		// 		Debug.Log(B.name + " : " + indexOfStartVertex.ToString());

        //         break;
        //     }

		// 	Config.Data.Edges.SelectEdge(AB);

		// 	indexOfEndVertex = Verticies.IndexOf(B);
		// }

		Config.Data.Paths.GeneratePathsFromSelectedEdges();
	}


	private void Cleanup()
	{
		Debug.Log("Cleaning up after Dijkstra alghorithm . . .");
		
        SelectedVerticies = null;
        Start = null;
        End = null;
        Verticies = null;
        Garbage = null;
        Edges = null;
        CurrentlyUsedVertex = null;
        PotentialVerticies = null;

        Recent = null;
        Weights = null;
	}
}
 