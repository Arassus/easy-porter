﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour 
{
	public Camera Camera;

	public float WheelSensitivity = 20f;

	public float MovementSpeed = 100f, MovementMax_X = 9.6f, MovementMax_Y = 4.75f;

	void Update_Movement()
	{
        float Movement_X = 0, Movement_Y = 0;

        Movement_X -= Config.Data.PorterInput.Camera.Movement.Left.IsClicked() && transform.position.x >= -MovementMax_X ? MovementSpeed * Time.deltaTime : 0f;
        Movement_X += Config.Data.PorterInput.Camera.Movement.Right.IsClicked() && transform.position.x <= MovementMax_X ? MovementSpeed * Time.deltaTime : 0f;

        Movement_Y -= Config.Data.PorterInput.Camera.Movement.Down.IsClicked() && transform.position.z >= -MovementMax_Y ? MovementSpeed * Time.deltaTime : 0f;
        Movement_Y += Config.Data.PorterInput.Camera.Movement.Up.IsClicked() && transform.position.z <= MovementMax_Y ? MovementSpeed * Time.deltaTime : 0f;

        Camera.transform.Translate(Movement_X, 0f, Movement_Y, Space.World);
	}

	void Update () 
	{
		if(Time.deltaTime > 0f)
		{
			Camera.fieldOfView += Input.GetAxis("Mouse ScrollWheel") * WheelSensitivity;

			Update_Movement();
		}
	}
}
