﻿Shader "Porter/Edge" 
{
	Properties
	{
		_MainColor("Main Color", Color) = (1,1,1,1)
		
		_SelectionColor("Selection Color", Color) = (1,1,1,1)

		_Selected("Selected", Range(0,1)) = 0

		_Path("Selected", Range(0,1)) = 0
	}
	SubShader 
	{
		CGPROGRAM

		#pragma surface surf Lambert

		float4 _MainColor,_SelectionColor;

		float _Selected;

		struct Input
		{
        	float4 color : COLOR;
		};

		void surf(Input IN, inout SurfaceOutput o)
		{
			if(_Selected == 1)
			{
				o.Emission = _SelectionColor;
			}
			else
			{
				o.Emission = _MainColor;
			}
		}

		ENDCG
	}
	FallBack "Diffuse"
}