﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeController : MonoBehaviour 
{
	public VertexController A, B;

	public LineRenderer Line;

	public float Lenght = 0f;

	public bool Selected = false;

	public int SelectedIndex = -1;

	bool Connected = false;

	private void OnEnable() 
	{
		Line = GetComponent<LineRenderer>();
	}

	public void Connect(VertexController A, VertexController B)
	{
		this.A = A;
		
		this.B = B;

		Line.SetPosition(0, A.transform.position);
		
		Line.SetPosition(1, B.transform.position);

		Connected = true;
	}

    LineRenderer line;
    CapsuleCollider capsule;

    public float LineWidth; // use the same as you set in the line renderer.

    void Start()
    {
        line = GetComponent<LineRenderer>();
        capsule = GetComponent<CapsuleCollider>();
        capsule.radius = 0.06f;
        capsule.center = Vector3.zero;
        capsule.direction = 2; // Z-axis for easier "LookAt" orientation
    }

    private void Update() 
	{
		if(Connected)
		{
			if(A != null && B != null)
			{
				Line.SetPosition(0, A.transform.position);
				
				Line.SetPosition(1, B.transform.position);

                capsule.transform.position = A.transform.position + (B.transform.position - A.transform.position) / 2;
                capsule.transform.LookAt(A.transform.position);
                capsule.height = (B.transform.position - A.transform.position).magnitude;
			}
			else
			{
				Config.Data.Edges.EdgeList.Remove(this);

				Destroy(gameObject);
			}
		}
	}

	public List<PathController> Paths = new List<PathController>();
}

