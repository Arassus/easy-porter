﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Config 
{
	public static DataController Data;

	public static MapUIConfig MapConfig;

	public static SelectionUIConfig SelectionConfig;
}
