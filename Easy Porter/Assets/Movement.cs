﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour 
{
	public float force;

	Rigidbody Rig;

	private void Start() {
		Rig = GetComponent<Rigidbody>();
	}

	void Update () 
	{
		if(Rig != null)
		{
			if(Input.GetKey(KeyCode.W))
			{
				Rig.AddForce(0f,0f,force);
			}

			if(Input.GetKey(KeyCode.S))
			{
				Rig.AddForce(0f,0f,-force);
			}
			if(Input.GetKey(KeyCode.A))
			{
				Rig.AddForce(-force,0f,0f);
			}

			if(Input.GetKey(KeyCode.D))
			{
				Rig.AddForce(force,0f,0f);
			}
		}	
	}
}
